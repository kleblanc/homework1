<p> 1. a. Confocal imaging data of mouse neurons. <br>
b. Data was collected using a confocal microscope. These mice have a YFP transgene that is expressed in Layer 5 pyramidal neurons in the cortex. The animals are perfused and the brains are fixed in PFA and then sliced using a vibratom into 150 micron sections. The sections are imaged at 64x magnification on a Lica confocal microscope. 
<br>
c. I collected this data myself as part of my thesis project in Carla Shatz's lab here at Stanford.
<br>
d. The data is not public.
<br>
e. There is not currently a paper.
<br>
f. Z-stacks of confocal images taken at 64x of neurons in the mouse visual cortex tagged with YFP for spine counting. 
<br>
<br>
2.a. The data is stored in lif files that contain multiple z stacks of images. LIF (Licra Image Files) are a type of file that is specific to images taken on a Licra microscope. The z stacks themselves can be saved as tiffs (which preserves the z stack) but lif files are what is produced when the images are taken on the confocal.
<br>	
b. I imaged and counted the brains while blinded, so the metadata attached to the files themselves is the day it was taken, the blinded name of the animal, the side of the brain that I was looking on (there are four hemispheres on each slide) and then the neuron and dendrite itself (represented by the position label) that was imaged. This would look like this example image name: Adele_Slide1a_Neuron1/Position001. In addition to this information, I also have a file with the unblinded sample numbers as well as all the necessary information about each of the mice including genotype, age, sex, date of birth, and notes about perfusion, sectioning, and imaging.
<br>
<br>
3.a. Each of the lif files represents a day of imaging (which I now realize was not the best way to store the data given that it's so hard to access brains). For this class, I will be using one day of data that has two brains in it (that represent one important genotype comparison). Both of the brains are contained in the single lif file named with the day I took the data (21/02/27) and then named according to the animal "name", brain side, neuron, and position (as can be seen above in the metadata). 
<br>
b. Inside the lif files, each of the images is stored as a collection of unint8 pixels. There are 2048x2048 pixels in the x and y directions and then between 40-100 pixels in the y direction depending on the width of the neuron being imaged.
<br>
<br>
4.a. The size of this file is 12.88 GB. 
<br>
b. The data is uncompressed (which is why I own an unfortunate number of 1 TB external hard drives).
<br>
c. Assumptions: 21 neurons, 60 slides per image, two images per neuron. Total images = 21 neurons * 2 positions = 42 images. Total pixels per image = 2048*2048*60 = 251658240 pixels. Total pixels for all images = 251658240 * 42 = 10569646080.  Each pixel is a byte, so 10569646080 pixels is 10569646080 bytes which is 10.57 GB. This is a bit smaller than the displayed value probably because I didn't take into account how the metadata and other metrics were stored.  
<br>
<br>
5.a. With image data like this analyses that would be good to do would be to count spines (either manually or with AI) or to assess brightness (in order to assess image quality).
<br>
b. Some summary statistics would be to sum or average pixel brightness over the z stack, quantify the amount of bridghtness in an image vs darkness, or calculating the average level of brightness in the pixels.
<br>
c. Visualizations might be the most important part of this data and would mostly involve showing the actual images, showing the images averaged over the entire z-stack, or being able to convert between different colors when showing the image.
</p>
